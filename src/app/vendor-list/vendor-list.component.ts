import { Component, OnInit } from '@angular/core';
import { MainService } from "../main.service";

@Component({
  selector: 'app-vendor-list',
  templateUrl: './vendor-list.component.html',
  styleUrls: ['./vendor-list.component.css']
})
export class VendorListComponent implements OnInit {
  vendorName:string;

  constructor(private data: MainService) { }

  ngOnInit() {
    this.data.currentVendor.subscribe(vendor => this.vendorName = vendor)

    // var importVendor=document.getElementById('importVendor');
    // importVendor.addEventListener('click',function () {
    //   document.getElementById('selectOption').value;
    // 
    // })

  
}

addVendor() {
  let vendorFileName:any = (<HTMLInputElement>document.getElementById('selectOption')).value;
  this.data.addVendor(vendorFileName)
}

}
