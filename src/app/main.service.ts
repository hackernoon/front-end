import { Injectable } from '@angular/core';
import { BehaviorSubject,Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class MainService {
  private vendorName = new BehaviorSubject<string>('');
  currentVendor = this.vendorName.asObservable();

  private objName = new BehaviorSubject<Object>({});
  currentObj = this.objName.asObservable()

  private arrName = new BehaviorSubject<Array<Object>>([]);
  currentArr = this.arrName.asObservable()

  constructor(private http:HttpClient) { }

  addVendor(vendorFileName: string) {
    this.vendorName.next(vendorFileName)
  }

  generateNote(anatomicName:string,vendorName:string,action:string) {
    let obj:Object={
      action:action,
      vendorName:vendorName,
      anatomicName:anatomicName,
      time:Date()
    }
    this.objName.next(obj);
  }

  sendArray(arr: Object[]) {
    this.arrName.next(arr);
    this.postAllNotes(JSON.stringify(this.arrName.value));
    return this.http.post('/api/gnote',{data:arr});
  }

  getNotes() {
    return (<any>this.http.get('/api/gnote'));
  }

  postAllNotes(data) {
  }

}

