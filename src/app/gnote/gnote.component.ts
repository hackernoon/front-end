import { Component, OnInit } from '@angular/core';
import { MainService } from "../main.service";
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { post } from 'selenium-webdriver/http';
import * as BABYLON from 'babylonjs';

@Component({
  selector: 'app-gnote',
  templateUrl: './gnote.component.html',
  styleUrls: ['./gnote.component.css']
})

export class GNoteComponent implements OnInit {
  private scene: BABYLON.Scene;

  private allNotes : Object[] = [];
  public notes;

  constructor(public _demoService:MainService,private data: MainService) { }

  ngOnInit() {
    // this.allNotes = this.getNotes();
    this.getNotes();
    this.data.currentObj.subscribe(
      (object) => {
       this.allNotes.push(object)
      }
    )
    console.log(this.allNotes,' here all ');
  }
 
  public noteInfo(object) : void {  }

  public getNotes()  {    
    this._demoService.getNotes().subscribe(
      data => { this.notes = data;
        console.log('data ',data);
        if(this.notes[this.notes.length-1]['anatomicName']==='Add'){
          BABYLON.SceneLoader.ImportMesh("","assets/", 'Bracket_3.babylon', this.scene, (result) => {
          })
        }
    },
      // the second argument is a function which runs on error
      err => console.error(err),
      // the third argument is a function which runs on completion
      () => console.log('done loading notes')
    );
  }

  // public postAllNotes() : void {
  //   this._demoService.postAllNotes(this.allNotes).subscribe(
  //     // the first argument is a function which runs on success
  //     data => { this.notes = data},
  //     // the second argument is a function which runs on error
  //     err => console.error(err),
  //     // the third argument is a function which runs on completion
  //     () => console.log('done loading notes')
  //   );
  // }

  public saveAll() : void {
    this.data.sendArray(this.allNotes);
  }

}
