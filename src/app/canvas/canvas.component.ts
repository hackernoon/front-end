import { Component, OnInit } from '@angular/core';
import { MainService } from "../main.service";
import * as BABYLON from 'babylonjs';
// import { modelGroupProvider } from '@angular/forms';
@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})

export class CanvasComponent implements OnInit {
  private canvas: any;
  private engine: BABYLON.Engine;
  private camera: BABYLON.ArcRotateCamera;
  private scene: BABYLON.Scene;
  private light: BABYLON.Light;
  vendor:string;
  private pickResult: BABYLON.PickingInfo | undefined;

  constructor(private data: MainService) {  }

  ngOnInit() {
    this.data.currentVendor.subscribe(
      (vendor) => {
        // this.canvas?this.import(vendor):false;
        if(this.canvas && !this.scene.meshes[2]){
          this.import(vendor)
          console.log(this.scene.meshes)
        } else {false}
      }
    )

    this.canvas = document.getElementById('renderCanvas');
    this.engine = new BABYLON.Engine(this.canvas, true);
    this.canvas.width = window.innerWidth * .9;
    this.canvas.height = window.innerHeight / 2;
    this.createScene();
    this.animate();    
    this.highLight();
  }

  public createScene(): void {
    this.scene = new BABYLON.Scene(this.engine);
    this.light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 1, 0), this.scene);
    this.camera = new BABYLON.ArcRotateCamera("Camera", -21, 1.5, 20, BABYLON.Vector3.Zero(), this.scene);
    this.canvas.style.backgroundColor = "white";
    this.camera.attachControl(this.canvas, true);

    // this.scene.clearColor = new BABYLON.Color4(0.7, 0.7, 0.7,0.8);

    this.canvas.addEventListener('contextmenu', (event)=> {
      let pickResult:any = this.scene.pick(this.scene.pointerX, this.scene.pointerY);      
      if(pickResult.hit && pickResult.pickedMesh.name!=='Upper_Teeth1'){
        let context:any= document.getElementById('context');
        context.style.left=event.clientX+10+'px';
        context.style.top=event.clientY+10+'px';
        context.style.display='block';      
        document.getElementById('context2').style.display='none';
        
      }else{
        let context:any= document.getElementById('context2');
        context.style.left=event.clientX+10+'px';
        context.style.top=event.clientY+10+'px';
        context.style.display='block';      
        document.getElementById('context').style.display='none';
      }
      event.preventDefault();      
    }, false);

    this.canvas.addEventListener('click', (event)=> {
      let context:any= document.getElementById('context');
      let context2:any= document.getElementById('context2');
      context.style.display='none';
      context2.style.display='none';
      event.preventDefault();
    }, false);

    //    Central incisor_Babylon.babylon
    BABYLON.SceneLoader.ImportMesh("","assets/", "Tooth_2.babylon", this.scene, (result) => {
      result[0].scaling.x=60;
      result[0].scaling.y=60;
      result[0].scaling.z=60; 
    })
  }

  public import(vendorFileName):void {       
    BABYLON.SceneLoader.ImportMesh("","assets/", vendorFileName, this.scene, (result) => {
      result[0].scaling.x=60;
      result[0].scaling.y=60;
      result[0].scaling.z=60;      
      
    })
    this.addNote('Central incisor','SLBracket','Add') ;
  }

  public animate(): void {
    this.engine.runRenderLoop(() => {
      this.scene.render();
    });

    window.addEventListener('resize', () => {
        this.engine.resize();
    });
  }

  public highLight() : void {
    let lastMesh = null;
    this.canvas.addEventListener('mousemove', ()=>{
      this.pickResult = this.scene.pick(this.scene.pointerX, this.scene.pointerY);
      if(this.pickResult.hit && !lastMesh){
        // this.scene.effectLayers.length === 0 ||
        let hl = new BABYLON.HighlightLayer("hl1", this.scene);
        hl.addMesh(<any>this.pickResult.pickedMesh, BABYLON.Color3.Green());
        lastMesh = this.pickResult.pickedMesh.name;
      }
      if(!this.pickResult.hit || this.pickResult.pickedMesh.name !== lastMesh) {
        this.scene.effectLayers = [];
        lastMesh = null;
      }
    })
  }

  public removeVendor() : void {
    let pickResult:any = this.scene.pick(this.scene.pointerX, this.scene.pointerY);
    if(pickResult.pickedMesh.name !== "Upper_Teeth1" && pickResult.pickedMesh.name !== "Central_incisor_11"){
      if(window.confirm('Are you sure ?')){
        this.addNote('Central incisor','SLBracket','Remove') ;
        if(pickResult.pickedMesh.parent!==undefined){
          pickResult.pickedMesh.parent.dispose();
        }else{
          pickResult.pickedMesh.dispose();
        } 
      }
  }
}
  
  public addNote(anatomicName:string,vendorName:string,action:string) : void {
    this.data.generateNote(anatomicName,vendorName,action);
  }
}


